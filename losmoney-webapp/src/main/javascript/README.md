To proceed with luxurious frontend development follow this steps:

1. Go to https://nodejs.org/en/ to download and install latest node.js with 'added to PATH' option.
2. Go to javascript folder and execute 'npm install' and 'npm install -g gulp'
3. Execute 'gulp' from javascript folder (Probably will need to relaunch cmd after step 2)
4. PROFIT!

You now have automatically refreshing resources on save which automatically appear in src/resources folder for Spring
Boot app. So on launch all your changes to JS/JSX/HTML/CSS will appear in .jar under localhost:8080 (if not overridden)

Gulp Dev server address is: localhost:3333
