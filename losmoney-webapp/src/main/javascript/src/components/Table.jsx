"use strict";
import React from "react";
import Logger from "js-logger";
import TableRow from "../view/TableRow.jsx";
import EmployeesActions from "../action/EmployeesActions";
import EmployeesStore from "../store/EmployeesStore";
import connectToStores from "alt-utils/lib/connectToStores";

const LOG = Logger.get('Table');

export default class Table extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            employees: []
        };
    }

    static getStores() {
        return [EmployeesStore];
    }

    static getPropsFromStores() {
        return {
            employees: EmployeesStore.getState().employees
        };
    }

    render() {
        return (
            <table>
                <tbody>
                <tr>
                    <th>Name</th>
                    <th>Role</th>
                    <th>Hired</th>
                </tr>
                {this.props.employees.map((employee) => {
                    return <TableRow key={employee.name} employee={employee}/>;
                })}
                <tr>
                    <th>
                        <button onClick={this.onButtonClick}>Fetch</button>
                    </th>
                    <th>
                        <button onClick={this.onInfoRequest}>Log objects</button>
                    </th>
                </tr>
                </tbody>
            </table>

        );
    }

    onButtonClick() {
        EmployeesActions.fetchEmployees();
    }

    onInfoRequest() {
    }

}

export default connectToStores(Table);