"use strict";
import Alt from "../alt";
import Logger from "js-logger";
import EmployeesActions from "../action/EmployeesActions.js";

const LOG = Logger.get("EmployeesStore");

class EmployeesStore {

    constructor() {
        this.bindActions(EmployeesActions);
        this.employees = [];
    }

    fetchEmployees(employees) {
        this.employees = employees;
        LOG.debug(`Fetched ${employees.length} employees objects`);
    }

    updateEmployee(employee) {
        let index = this.employees.findIndex(
            function (e) {
                return e.name === employee.name;
            }, employee);
        LOG.debug(employee);
        this.employees[index] = employee;
    }

}


export default Alt.createStore(EmployeesStore, 'EmployeesStore');