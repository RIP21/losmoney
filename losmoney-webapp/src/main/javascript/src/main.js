"use strict";
import React from "react";
import ReactDOM from "react-dom";
import Table from "./components/Table.jsx";
import Logger from "js-logger";

function globalLoggerConfig() {
    Logger.useDefaults();
    Logger.setLevel(Logger.DEBUG); // INFO, DEBUG, WARN, ERROR, OFF
}

globalLoggerConfig();
ReactDOM.render(
    <Table/>
    , document.getElementById('container'));