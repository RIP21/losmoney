"use strict";
import Alt from "../alt";
import Logger from "js-logger";
import {data} from "../datasource/Data.js";

const LOG = Logger.get("EmployeesActions");

class EmployeesActions {

    fetchEmployees() {
        LOG.debug(data.getEmployees());
        return data.getEmployees();
    }

    updateEmployee(employee, attr, value) {
        LOG.debug(employee.name + ' updating...');
        employee[attr] = value;
        return employee;
    }

}

export default Alt.createActions(EmployeesActions);