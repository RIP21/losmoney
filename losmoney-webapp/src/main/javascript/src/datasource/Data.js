"use strict";
class Data {

    getEmployees() {
        return [{
            "name": "John",
            role: "1",
            hired: true
        }, {
            "name": "Piotr",
            role: "2",
            hired: true
        }, {
            "name": "Andrii",
            role: "1",
            hired: true
        }];
    }
}

export const data = new Data();