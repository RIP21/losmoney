import React from "react";
import Logger from "js-logger";
import EmployeesActions from "../action/EmployeesActions";

const LOG = Logger.get('RoleSelect');

export default class RoleSelect extends React.Component {

    render() {
        return (
            <select value={this.props.employee.role} onChange={this.onSelectChange.bind(this)}>
                <option value="1">Manager</option>
                <option value="2">Programmer</option>
            </select>
        );
    }

    onSelectChange(event) {
        LOG.debug('On Select Event was triggered');
        EmployeesActions.updateEmployee(this.props.employee, 'role', event.target.options[event.target.selectedIndex].value);
    }
}