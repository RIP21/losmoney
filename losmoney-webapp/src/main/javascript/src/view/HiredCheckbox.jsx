import React from "react";
import Logger from "js-logger";
import EmployeesAction from "../action/EmployeesActions";

const LOG = Logger.get("EmployeesStore");

export default class HiredCheckbox extends React.Component {

    render() {
        return (
            <input type="checkbox" checked={this.props.employee.hired} onChange={this.onCheckBoxClicked.bind(this)}/>
        );
    }

    onCheckBoxClicked(event) {
        LOG.debug('On Checkbox Event was triggered');
        LOG.debug(event.target.checked);
        EmployeesAction.updateEmployee(this.props.employee, 'hired', event.target.checked);
    }

}