"use strict";
import React from "react";
import Logger from "js-logger";
import RoleSelect from "./RoleSelect.jsx";
import HiredCheckbox from "./HiredCheckbox.jsx";

const LOG = Logger.get('TableRow');

export default class TableRow extends React.Component {

    render() {
        return (
            <tr>
                <td>{this.props.employee.name}</td>
                <td><RoleSelect employee={this.props.employee}/></td>
                <td><HiredCheckbox employee={this.props.employee}/></td>
            </tr>
        );
    }
}
