package com.losmoney;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LosmoneyWebappApplication {

	public static void main(String[] args) {
		SpringApplication.run(LosmoneyWebappApplication.class, args);
	}
}

